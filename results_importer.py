#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *    Clayton Craft <clayton.a.craft@intel.com>
#  *    Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/

import datetime
import glob
import hashlib
import json
import os
import shutil
import subprocess
import tempfile
import time
import warnings
import xml.etree.cElementTree as et

import MySQLdb
import inotify.adapters


max_builds = {
    'default':              20,
    'mesa_master':          500,
    'vulkancts':            500,
    'mesa_master_daily':    75,
    'vulkancts_daily':      75,
    'mesa_custom':          75,
}

def md5(t_string):
    """ Return the md5 hash of the given str """
    md5sum = hashlib.md5()
    md5sum.update(t_string.encode())
    return md5sum.hexdigest()


class Tarball:
    """Extracts a tarball for import into the results database"""
    def __init__(self, tarfile):
        self.tarfile = tarfile
        self.tmpdir = None

    def __enter__(self):
        self.tmpdir = tempfile.mkdtemp()
        return self

    def __exit__(self, _type, value, traceback):
        os.remove(self.tarfile)
        shutil.rmtree(self.tmpdir)

    def process(self):
        """top level entry point for handling a tarball"""
        self._extract()
        build_info = self._get_build_info(self.tmpdir + '/build_info.json')
        if not build_info:
            raise RuntimeError(f"ERR: Unable to process build_info for tar file: {self.tarfile}")
        return build_info, self.tmpdir

    def _extract(self):
        try:
            subprocess.check_output(['tar', 'xf', self.tarfile,
                                     '-C', self.tmpdir])
        except subprocess.CalledProcessError:
            raise RuntimeError(f"WARN: Unable to extract tar file: {self.tarfile}")

    def _get_build_info(self, build_info_file):
        build_info = None
        try:
            with open(build_info_file, 'r') as info_fh:
                build_info = json.load(info_fh)
        except FileNotFoundError:
            print(f"WARN: build_info json not found: {build_info_file}")
        return build_info


class ResultsImporter:
    """Watches the import directory and handles each new import tarball"""
    def __init__(self, watch_dir, sql_socket=None, sql_user='jenkins',
                 sql_host='localhost'):
        self.sql_pw = ''
        self.sql_user = sql_user
        self.sql_host = sql_host
        self.sql_socket = sql_socket
        self.watch_dir = watch_dir
        if "SQL_DATABASE_PW_FILE" in os.environ:
            sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
            if os.path.exists(sql_pw_file):
                with open(sql_pw_file, 'r') as pw_fh:
                    self.sql_pw = pw_fh.read().rstrip()

        self.inot = inotify.adapters.Inotify()
        self.inot.add_watch(self.watch_dir)

        self.build_info = None
        self.resultdir = None

    def poll(self):
        """ Blocks until a new tarball is found by inotify, then
        returns the path when one is found """
        for event in self.inot.event_gen(yield_nones=False):
            (_, type_names, path, filename) = event
            new_file = path + "/" + filename
            # Only proceed if the event was close after write
            if 'IN_CLOSE_WRITE' not in type_names:
                continue
            if not new_file or not os.path.exists(new_file):
                continue
            return new_file

    def consume(self, tarfile):
        """ Import data in given tarfile """
        with Tarball(tarfile) as tarball:
            try:
                self.build_info, self.resultdir = tarball.process()
            except RuntimeError:
                print("Skipping broken tar...")
                return
            print(f"Importing build: {self.build_info['name']}")
            with ImportSql(host=self.sql_host, user=self.sql_user,
                           password=self.sql_pw, socket=self.sql_socket,
                           resultdir=self.resultdir,
                           start_time=self.build_info['start_time'],
                           end_time=self.build_info['end_time'],
                           url=self.build_info['url'],
                           build_num=self.build_info["build"],
                           build_name=self.build_info["name"],
                           job=self.build_info["job"],
                           result_path=self.build_info["result_path"]) as importer:
                importer.add_revisions(self.build_info["revisions"])
                count = 1
                for component in self.build_info['components']:
                    importer.add_component(component)
                    print(f"Imported {count} of {len(self.build_info['components'])}")
                    count += 1
                importer.set_imported()
                importer.prune()
        print(f"Done processing tarball: {tarfile}")


class Test:
    """Encapsulates the details for a single test result, parsed from junit"""
    def __init__(self, etree):
        test_name = etree.attrib["name"]
        if "classname" in etree.attrib:
            test_name = ".".join([etree.attrib["classname"],
                                  etree.attrib["name"]])
        name_components = test_name.split(".")
        platform = name_components[-1]
        self.arch = platform[-3:]
        self.hardware = platform[:-3]
        self.name = ".".join(name_components[:-1])
        self.flaky_count = 0
        self.disabled = False
        self._id = md5(self.name)

        # status attribute stores the test result as produced by the suite
        if "status" in etree.attrib:
            self.status = etree.attrib["status"]
        else:
            self.status = "pass"
        if self.status == "crash":
            self.status = "fail"
        self.filtered_status = self.status

        self.flaky_miss_count = 0
        if self.status == "pass":
            self.pass_count = 1
            self.fail_count = 0
        else:
            self.pass_count = 0
            self.fail_count = 1

        # mesa ci overwrites fail/skip/error tags base on test status
        # in config files
        if etree.find("failure") is not None:
            self.filtered_status = "fail"
        elif etree.find("error") is not None:
            self.filtered_status = "fail"
        elif etree.find("skipped") is not None:
            self.filtered_status = "skip"
        else:
            self.filtered_status = "pass"
        self._stderr = ""
        err = etree.find("system-err")
        if err is not None and err.text is not None:
            self._stderr = err.text
        self._stdout = ""
        out = etree.find("system-out")
        if out is not None and out.text is not None:
            self._stdout = out.text

        self.time = 0.0
        if "time" in etree.attrib:
            self.time = float(etree.attrib["time"])

    def result_row(self, out_rows):
        """provide ordered data for populating a single row in the result database"""
        out_rows.append((self._id, self.hardware, self.arch,
                         self.status, self.filtered_status,
                         self.pass_count, self.fail_count, self.flaky_miss_count,
                         False, self.time, self._stdout, self._stderr))


class FlakyTest:
    """Encapsulates the details for a single test result, parsed from flaky xml"""
    def __init__(self, arch, hardware, etree):
        self.name = etree.attrib["name"]
        self.time = 0.0
        self.arch = arch
        self.hardware = hardware
        self.flaky_count = 1
        self._id = md5(self.name)
        self._stderr = ""
        self._stdout = ""
        self.filtered_status = "pass"
        self.disabled = False
        self.status = "pass"
        if etree.attrib["expected"] != "pass":
            # expected random and fail tests will have status skip by default
            self.filtered_status = "skip"
            self.status = "fail"
        self.flaky_miss_count = int(etree.attrib["miss"])

        # non-random tests should not be flaky more than once
        if etree.attrib["expected"] != "random" and self.flaky_miss_count > 1:
            self.filtered_status = "fail"
            self.status = "fail"
            if etree.attrib["expected"] == "fail":
                self.status = "pass"

        self.pass_count = int(etree.attrib["match"])
        self.fail_count = self.flaky_miss_count
        if etree.attrib["expected"] == "fail":
            self.pass_count, self.fail_count = self.fail_count, self.pass_count

        out = etree.find("output")
        if out is not None and out.text is not None:
            self._stdout += out.text

    def result_row(self, out_rows):
        """Provide a row of results that can be added to the result table"""
        out_rows.append((self._id, self.hardware, self.arch,
                         self.status, self.filtered_status,
                         self.pass_count, self.fail_count, self.flaky_miss_count,
                         False, self.time, self._stdout, self._stderr))

class DisabledTest:
    """Encapsulates the details for a single test result, parsed from disabled xml"""
    def __init__(self, arch, hardware, etree):
        self.name = etree.attrib["name"]
        self.arch = arch
        self.hardware = hardware
        self.disabled = True
        self._id = md5(self.name)
        self.time = 0.0
        self.flaky_count = 0
        self.filtered_status = "skip"
        self.status = "skip"

    def result_row(self, out_rows):
        """Provide a row of results that can be added to the result table"""
        out_rows.append((self._id, self.hardware, self.arch,
                         "skip", # status
                         "skip", # filtered_status
                         0, # pass_count
                         0, # fail_count
                         0, # flaky_miss_count
                         True, # disabled
                         0.0, #time
                         "", # stdout
                         "")) # stderr


class TestTree:
    """"Recursive tree data structure to accumulate and represent rows
    in the group database"""
    def __init__(self, groupname="root"):
        self._groupname = groupname
        self._group_id = md5(groupname)

        # key is final test component (name.hwarch), value is Test object
        self._tests = {}
        self._subgroups = {}
        self._hardware = {}
        self._flaky_count = 0
        self._disabled_count = 0
        self._time = 0.0
        self._pass_count = 0
        self._filtered_pass_count = 0
        self._fail_count = 0

    def parse_flaky(self, xml):
        """Process a file of flaky results"""
        root_t = et.parse(xml).getroot()
        arch = root_t.attrib['arch']
        hardware = root_t.attrib['hardware']
        for test_tag in root_t.findall(".//test"):
            test_t = FlakyTest(etree=test_tag, arch=arch, hardware=hardware)
            self.add_test(test_t)

    def parse_disabled(self, xml):
        """Process a file of disabled results"""
        root_t = et.parse(xml).getroot()
        arch = root_t.attrib['arch']
        hardware = root_t.attrib['hardware']
        for test_tag in root_t.findall(".//test"):
            test_t = DisabledTest(etree=test_tag, arch=arch, hardware=hardware)
            self.add_test(test_t)

    def parse(self, xml):
        """Process a file of junit results"""
        root_t = et.parse(xml)
        for test_tag in root_t.findall(".//testcase"):
            test_t = Test(test_tag)
            self.add_test(test_t)

    def add_test(self, test):
        """add a single test result (xml etree tag format) to the
        TestTree, populating all 'group' nodes in the branches of the
        tree"""
        test_path = test.name.split(".")
        subgroup = test_path[0]
        sub_test_path = test_path[1:]
        if subgroup not in self._subgroups:
            self._subgroups[subgroup] = TestTree(subgroup)
        self._subgroups[subgroup]._add_test(sub_test_path, test)
        if test.hardware not in self._hardware:
            self._hardware[test.hardware] = self._Platform(test.hardware)
        hardware = self._hardware[test.hardware]
        self._time += test.time
        hardware.time += test.time
        self._flaky_count += test.flaky_count
        hardware.flaky_count += test.flaky_count
        if test.disabled:
            self._disabled_count += 1
            hardware.disabled_count += 1
        if test.filtered_status == "pass":
            self._pass_count += 1
            hardware.pass_count += 1
        if test.filtered_status == "fail":
            self._fail_count += 1
            hardware.fail_count += 1
        if test.filtered_status == "skip" and test.disabled is False:
            self._filtered_pass_count += 1
            hardware.filtered_pass_count += 1

    def _add_test(self, test_path, test):
        if test_path:
            subgroup = test_path[0]
            sub_test_path = test_path[1:]
            if subgroup not in self._subgroups:
                full_group = self._groupname + "." + subgroup
                self._subgroups[subgroup] = TestTree(full_group)
            self._subgroups[subgroup]._add_test(sub_test_path, test)
        else:
            self._tests[test.hardware + test.arch] = test
        if test.hardware not in self._hardware:
            self._hardware[test.hardware] = self._Platform(test.hardware)
        hardware = self._hardware[test.hardware]
        self._time += test.time
        hardware.time += test.time
        self._flaky_count += test.flaky_count
        hardware.flaky_count += test.flaky_count
        if test.disabled:
            self._disabled_count += 1
            hardware.disabled_count += 1
        if test.filtered_status == "pass":
            self._pass_count += 1
            hardware.pass_count += 1
        if test.filtered_status == "fail":
            self._fail_count += 1
            hardware.fail_count += 1
        if test.filtered_status == "skip" and test.disabled is False:
            self._filtered_pass_count += 1
            hardware.filtered_pass_count += 1

    def group_rows(self, _out_rows=None):
        """provide all of the data items (tuple format) for populating
        the group table, based on the contents of this TestTree"""
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows

        # last tuple value determines whether self is a test
        out_rows.append((self._group_id, self._pass_count, self._flaky_count, self._disabled_count,
                         self._fail_count, self._filtered_pass_count, self._time,
                         self._hardware, bool(self._tests)))
        for (_, subgroup) in self._subgroups.items():
            subgroup.group_rows(out_rows)

        # return value used in top-level call
        return out_rows

    def result_rows(self, _out_rows=None):
        """provide all of the data items (tuple format) for populating
        the result table, based on the contents of this TestTree"""
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows
        for (_, a_test) in self._tests.items():
            a_test.result_row(out_rows)
        for (_, subgroup) in self._subgroups.items():
            subgroup.result_rows(out_rows)

        # return value used in top-level call
        return out_rows

    def list_all_tests(self, _tests=None):
        """Provide a full list of all tests in the TestTree.  This
        functionality is necessary for determining if any new tests
        exist in the tree.  New tests require additional table
        entries."""
        tests = {}
        if _tests is not None:
            tests = _tests

        # query db to see if any test ids must be added
        # accessing __len__ directly is MUCH faster than len(dict)
        current_count = tests.__len__()
        for (_, subgroup) in self._subgroups.items():
            subgroup.list_all_tests(tests)

        if self._subgroups and tests.__len__() == current_count:
            # adding any of the subgroups would have also added this
            # group.
            return tests
        tests[self._group_id] = self._groupname
        return tests

    def new_tests(self, cursor, known_tests):
        """Provide a list of any tests in the TestTree, which do not
        exist in the know_tests dict.  New tests require additional
        table entries.
        """
        all_tests = self.list_all_tests()
        existing_tests = set.intersection(known_tests, set(all_tests.values()))
        for a_test in existing_tests:
            del all_tests[md5(a_test)]
        all_test_ids = [f"'{a}'" for a in all_tests]
        step = 1000
        # accessing __len__ directly is MUCH faster than len(dict)
        for i in range(0, all_tests.__len__(), step):
            cmd = ('select test_name from test where test_id=' +
                   '{}'.format(' or test_id='.join(all_test_ids[i:i+step])))
            cursor.execute(cmd)
            known_tests.update([t[0] for t in cursor.fetchall()])
        return [t for t in all_tests.values() if t not in known_tests]

    def test_rows(self, tests):
        """Provide the rows of tests for insertion into the test table"""
        out_rows = []
        for a_test in tests:
            out_rows.append((md5(a_test), a_test))
        return out_rows

    def parent_rows(self, tests, _out_rows=None):
        """Provide the rows of parent relationships for insertion into
        the parent table."""
        out_rows = []
        if _out_rows is not None:
            out_rows = _out_rows
        for a_t in tests:
            test_id = md5(a_t)
            groups = a_t.split(".")
            if len(groups) > 1:
                parent = ".".join(groups[:-1])
            elif a_t == "root":
                continue
            else:
                parent = "root"
            parent_id = md5(parent)
            out_rows.append((test_id, parent_id))
        return out_rows


    class _Platform:
        def __init__(self, hardware, pass_count=0, flaky_count=0, disabled_count=0,
                     fail_count=0, filtered_pass_count=0, _time=0.0):
            self.hardware = hardware
            self.pass_count = pass_count
            self.flaky_count = flaky_count
            self.fail_count = fail_count
            self.disabled_count = disabled_count
            self.filtered_pass_count = filtered_pass_count
            self.time = _time

class ImportSql:
    """Responsible for all SQL actions on the target database.
    Processess a complete tarball, removing duplicate or out-of-date
    builds and generating new build entries for display on the result
    site.
    """
    def __init__(self, resultdir, start_time, end_time, url, build_name,
                 build_num, job, result_path, host='localhost', user='jenkins',
                 password=None, socket=None):
        warnings.filterwarnings('ignore', category=MySQLdb.Warning)
        self.host = host
        self.user = user
        self.socket = socket
        self.password = password
        self.resultdir = resultdir
        self.build_num = build_num
        self.build_name = build_name
        self.db = None
        self.cur = None
        self.job = job
        self.job = self.job.replace("-", "_")
        self.job = self.job.replace(".", "_")
        self.url = url
        self.result_path = result_path
        self._known_tests = set()
        if not start_time:
            start_time = 515462400
        self.build_start_time = datetime.datetime.fromtimestamp(
            int(start_time)).strftime('%Y-%m-%d %H:%M:%S')
        self.build_end_time = datetime.datetime.fromtimestamp(
            int(end_time)).strftime('%Y-%m-%d %H:%M:%S')

    def __enter__(self):
        self.connect()
        self.create_db()
        # Remove any duplicate build in the database before adding this build
        self.remove_build(self.build_num)
        return self

    def __exit__(self, a_type, value, traceback):
        self.disconnect()

    def connect(self):
        """Connect to the SQL database using TCP or unix domain
        socket, with connection details as specified in the constructo
        arguments.
        """
        connected = False
        while not connected:
            try:
                if self.socket:
                    self.db = MySQLdb.connect(user=self.user,
                                              unix_socket=self.socket,
                                              passwd=self.password,
                                              use_unicode=True, charset="utf8")
                else:
                    self.db = MySQLdb.connect(host=self.host, user=self.user,
                                              passwd=self.password,
                                              use_unicode=True, charset="utf8")
                connected = True
            except MySQLdb._exceptions.OperationalError:
                print("WARN: Unable to connect to sql server, retrying in 5 "
                      "seconds...")
                time.sleep(5)

        self.db.set_character_set('utf8')
        self.cur = self.db.cursor()
        self.cur.execute('SET NAMES utf8;')
        self.cur.execute('SET CHARACTER SET utf8;')
        self.cur.execute('SET character_set_connection=utf8;')

    def create_db(self):
        """If the target database does not exist, then create it and
        initialize all tables."""
        # '-' and '.' not allowed in database names
        self.cur.execute(f"create database if not exists {self.job}")
        self.cur.execute(f"use {self.job}")
        self.cur.execute("SET @@session.unique_checks = 0")
        self.cur.execute("SET @@session.foreign_key_checks = 0")
        self.db.commit()

        # build table
        self.cur.execute("create table if not exists build ("
                         "build_id int not null,  "
                         "build_name char(100), "
                         "pass_count int, "
                         "fail_count int, "
                         "flaky_count int not null, "
                         "disabled_count int not null, "
                         "start_time datetime, "
                         "end_time datetime, "
                         "url char(128) not null, "
                         "filtered_pass_count int,"
                         "result_path varchar(1024), "
                         "imported bool, "
                         "primary key(build_id));")
        # test table
        self.cur.execute("create table if not exists test ("
                         "test_id char(32) not null, "
                         "test_name text(1024) not NULL, "
                         "primary key(test_id))")
        # parent table
        self.cur.execute("create table if not exists parent ("
                         "test_id char(32) not null, "
                         "parent_id char(32) not null, "
                         "primary key(test_id), "
                         "index parent_id(parent_id))")
        # result table
        self.cur.execute("create table if not exists result ("
                         "result_id bigint unsigned not null AUTO_INCREMENT,  "
                         "test_id char(32) not null, "
                         "arch enum('m32', 'm64') not null, "
                         "hardware char(32), "
                         "build_id int not null, "
                         "status enum('pass', 'fail', 'skip') not null, "
                         "filtered_status enum('pass', 'fail', 'skip') not null, "
                         "flaky_miss_count int not null, "
                         "disabled BOOL not null, "
                         "time decimal(32,6), "
                         "stdout text, "
                         "stderr text, "
                         "pass_count int not null,"
                         "fail_count int not null,"
                         "primary key(result_id), "
                         "index build_id(build_id), "
                         "index test_id(test_id), "
                         "index filtered_status(filtered_status), "
                         "index build_test_hw_arch_status(build_id, test_id, "
                         "hardware, arch, status))")
        # group_ table
        self.cur.execute("create table if not exists group_ ("
                         "build_id int not null,  "
                         "test_id char(32) not null, "
                         "pass_count int not null, "
                         "flaky_count int not null, "
                         "disabled_count int not null, "
                         "fail_count int not null, "
                         "filtered_pass_count int not null, "
                         "time decimal(32,6) not null, "
                         "primary key(build_id, test_id))")
        # platform table
        self.cur.execute("create table if not exists platform ("
                         "hardware char(32) not null, "
                         "group_id char(32) not null, "
                         "build_id int not null,  "
                         "pass_count int not null, "
                         "flaky_count int not null, "
                         "disabled_count int not null, "
                         "fail_count int not null, "
                         "filtered_pass_count int not null, "
                         "time decimal(32,6) not null, "
                         "primary key(hardware, group_id, build_id), "
                         "index build_group(build_id, group_id), "
                         "index build_group_platform(build_id, group_id, hardware))")
        # artifact table
        self.cur.execute("create table if not exists artifact ("
                         "artifact_id int not null AUTO_INCREMENT, "
                         "type char(64) not null, "
                         "filename char(120) not null, "
                         "data longtext not null, "
                         "component_id int not null,"
                         "index component_id(component_id),"
                         "primary key(artifact_id))")
        # component table
        self.cur.execute("create table if not exists component ("
                         "component_id int not null AUTO_INCREMENT, "
                         "arch enum('m32', 'm64') not null, "
                         "status enum('success', 'failure', 'unstable', 'aborted') not null, "
                         "component_name char(100), "
                         "shard char(32), "
                         "machine char(64), "
                         "start_time datetime, "
                         "end_time datetime, "
                         "build int not null, "
                         "build_id int not null, "
                         "primary key(component_id))")
        # revision table
        self.cur.execute("create table if not exists revision ("
                         "revision_id int not null AUTO_INCREMENT, "
                         "project char(64) not null, "
                         "commit char(64) not null, "
                         "author char(64) not null, "
                         "description varchar(512) not null, "
                         "sha char(32) not null, "
                         "build_id int not null, "
                         "index build_id(build_id), "
                         "primary key(revision_id))")
        self.db.commit()

    def q(self, t_s, escape=False):
        """Quote the string for use in the database fields"""
        if not t_s:
            t_s = 'NULL'
        if escape and len(t_s):
            t_s = self.db.escape_string(t_s)
        try:
            return '"' + t_s + '"'
        except TypeError:
            return '"' + t_s.decode() + '"'

    def add_component(self, component):
        """Add the component into the component table"""
        start_time = datetime.datetime.fromtimestamp(
            int(component['start_time'])).strftime('%Y-%m-%d %H:%M:%S')
        # note: end_time may be null if the component failed
        if component['end_time']:
            end_time = datetime.datetime.fromtimestamp(
                int(component['end_time'])).strftime('%Y-%m-%d %H:%M:%S')
        else:
            end_time = start_time
        if 'status' not in component or not component['status']:
            status = 'aborted'
        else:
            status = component['status']
        # These values are allowed to be null
        machine = component['machine'] if 'machine' in component else ''
        shard = component['shard'] if 'shard' in component else ''
        try:
            arch = self.q(component['arch'])
            component_name = self.q(component['name'])
            shard = self.q(shard)
            machine = self.q(machine)
            start_time = self.q(start_time)
            end_time = self.q(end_time)
            build_id = self.build_num
            build = component['build']
            status = self.q(status)
            self.cur.execute("insert into "
                             "component( arch,   component_name,   shard,   machine, "
                             "  start_time,   end_time,   build_id,   build,   status ) "
                             f"values  ({arch}, {component_name}, {shard}, {machine}, "
                             f"{start_time}, {end_time}, {build_id}, {build}, {status})")
        except MySQLdb._exceptions.DataError:
            print("WARN: Cannot import component (results may be "
                  "incomplete?): " + "_".join([
                      str(component['name']),
                      str(component['arch']),
                      str(machine),
                      str(shard),
                  ]))
            return

        self.db.commit()
        self.cur.execute("select last_insert_id()")
        component_id = self.cur.fetchone()[0]
        if 'artifacts' in component:
            for artifact in component['artifacts']:
                if artifact['type'] == 'junit':
                    self.import_xml(self.resultdir + "/" + artifact['file'])
                elif artifact['type'] == 'flaky':
                    self.import_flaky(self.resultdir + "/" + artifact['file'])
                elif artifact['type'] == 'disabled':
                    self.import_disabled(self.resultdir + "/" + artifact['file'])
                else:
                    # note: currently only support importing text (not binary)
                    with open(self.resultdir + '/'
                              + artifact['file'], 'r') as a_f:
                        data = a_f.read()
                        data = self.q(data, escape=True)
                    _type = self.q(artifact['type'])
                    filename = self.q(artifact['file'])
                    self.cur.execute("insert into "
                                     "artifact(  type,   filename,   component_id,   data ) "
                                     f"values ({_type}, {filename}, {component_id}, {data})")
                    self.db.commit()

    def remove_build(self, build_num, keep_summary=False):
        """Clean out results table"""
        self.cur.execute(f'select count(*) from result where build_id="{build_num}"')
        count = int(self.cur.fetchone()[0])
        if count == 0:
            # this build was already deleted, but the summary remained
            return False
        while count > 0:
            print("deleting results from matching build: " + str(count))
            self.cur.execute(f"delete quick from result where build_id={build_num} limit 100000")
            self.db.commit()
            self.cur.execute(f'select count(*) from result where build_id="{build_num}"')
            count = int(self.cur.fetchone()[0])
        # Clean out group_ table
        self.cur.execute(f'select count(*) from group_ where build_id="{build_num}"')
        count = int(self.cur.fetchone()[0])
        while count > 0:
            print("deleting group stats from matching build: " + str(count))
            self.cur.execute(f"delete quick from group_ where build_id={build_num} limit 100000")
            self.db.commit()
            self.cur.execute(f'select count(*) from group_ where build_id="{build_num}"')
            count = int(self.cur.fetchone()[0])
        # Clean out platform table
        self.cur.execute(f'select count(*) from platform where build_id="{build_num}"')
        count = int(self.cur.fetchone()[0])
        while count > 0:
            print("deleting platform stats from matching build: " + str(count))
            self.cur.execute(f"delete quick from platform where build_id={build_num} limit 100000")
            self.db.commit()
            self.cur.execute(f'select count(*) from platform where build_id="{build_num}"')
            count = int(self.cur.fetchone()[0])
        # Clean out components table
        self.cur.execute(f'select component_id from component where build_id="{build_num}"')
        for component_id in self.cur.fetchall():
            # Clean out artifacts table
            self.cur.execute("delete quick from artifact where "
                             "component_id=%s", [component_id])
            self.db.commit()
        self.cur.execute(f'select count(*) from component where build_id="{build_num}"')
        count = int(self.cur.fetchone()[0])
        print(f"deleting components from matching build: {str(count)}")
        self.cur.execute(f"delete quick from component where build_id={build_num} limit 100000")
        self.db.commit()
        print(f"deleting revisions for build: {build_num}")
        self.cur.execute(f'delete quick from revision where build_id="{build_num}"')
        self.db.commit()
        if keep_summary:
            print("retaining build information to support summary statistics")
        else:
            print(f"deleting build: {build_num}")
            self.cur.execute('delete quick from build where build_id="{build_num}"')
            self.db.commit()
        return True

    def prune(self):
        """Remove builds if too many results have accumulated"""
        self.cur.execute("select count(*) from build")
        count = int(self.cur.fetchone()[0])
        if self.job in max_builds:
            count_max = max_builds[self.job]
        else:
            count_max = max_builds['default']

        removed_builds = 0
        self.cur.execute("select build_id from build order by build_id asc")
        for prune_build in self.cur.fetchall():
            if self.remove_build(prune_build[0], keep_summary=True):
                removed_builds += 1
                if removed_builds >=3:
                    # Don't prune more than 3 builds at a time, to help with import latency
                    return
            # else the build was already removed (only the summary
            # remains).  It does not count against the build limit.
            count -= 1
            if count <= count_max:
                # build count is within the limit
                return

    def import_flaky(self, xml_file):
        """import an xml file with flaky results into the database"""
        print(f"importing flaky file: {xml_file}")
        t_t = TestTree()
        try:
            t_t.parse_flaky(xml_file)
        except SyntaxError:
            # test does not have valid junit
            print("INFO: flaky file is not valid, skipping")
            return
        self.import_test_tree(t_t)

    def import_disabled(self, xml_file):
        """import an xml file with disabled results into the database"""
        print(f"importing disabled file: {xml_file}")
        t_t = TestTree()
        try:
            t_t.parse_disabled(xml_file)
        except SyntaxError:
            # test does not have valid junit
            print("INFO: disabled file is not valid, skipping")
            return
        self.import_test_tree(t_t)

    def import_xml(self, xml_file):
        """import an xml file with junit results into the database"""
        print(f"importing junit file: {xml_file}")
        t_t = TestTree()
        try:
            t_t.parse(xml_file)
        except SyntaxError:
            # test does not have valid junit
            print("INFO: junit file is not valid, skipping")
            return
        self.import_test_tree(t_t)

    def import_test_tree(self, t_t):
        """import the test tree into all result tables"""
        new_tests = t_t.new_tests(self.cur, self._known_tests)
        if new_tests:
            print(f"found {len(new_tests)} new tests")
            test_rows = t_t.test_rows(new_tests)
            values = [f"({self.q(id)}, {self.q(name)})" for (id, name) in test_rows]
            self.cur.execute('insert ignore into test '
                             f'(test_id, test_name) values {", ".join(values)}')

            parent_rows = t_t.parent_rows(new_tests)
            values = [f"({self.q(id)}, {self.q(parent)})" for (id, parent) in parent_rows]
            values = ", ".join(values)
            self.cur.execute(f"insert ignore into parent (test_id, parent_id) values {values}")
            self.db.commit()

        print("updating group statistics")
        groups = t_t.group_rows()
        group_values = []
        platform_values = []
        for (test_id, pass_count, flaky_count, disabled_count,
             fail_count, filtered_pass_count,
             _time, platforms, is_test) in groups:
            group_values.append(f"({self.build_num}, {self.q(test_id)}, {pass_count}, "
                                f"{flaky_count}, {disabled_count}, {fail_count}, "
                                f"{filtered_pass_count}, {_time})")
            if is_test:
                continue
            test_id = self.q(test_id)
            for plat in platforms.values():
                hardware = self.q(plat.hardware)
                platform_values.append(f"({hardware}, {test_id}, {self.build_num}, "
                                       f"{plat.pass_count}, {plat.flaky_count}, "
                                       f"{plat.disabled_count}, {plat.fail_count}, "
                                       f"{plat.filtered_pass_count}, {plat.time})")
        group_values = ", ".join(group_values)
        self.cur.execute("insert into "
                         "group_ (build_id, test_id, pass_count, flaky_count, "
                         "disabled_count, fail_count, filtered_pass_count, time) "
                         f"values {group_values} " +
                         "on duplicate key update "
                         "pass_count = pass_count + values(pass_count), "
                         "flaky_count = flaky_count + values(flaky_count), "
                         "disabled_count = disabled_count + values(disabled_count), "
                         "fail_count = fail_count + values(fail_count), "
                         "filtered_pass_count = filtered_pass_count + values(filtered_pass_count), "
                         "time = time + values(time)")
        self.db.commit()
        print("updating platform statistics")
        # protect against empty junit testcase (e.g. shard crashed but still
        # generated 'results')
        if platform_values:
            self.cur.execute("insert into "
                             "platform (hardware, group_id, build_id, pass_count, flaky_count, "
                             "disabled_count, fail_count, filtered_pass_count, time) "
                             f"values {', '.join(platform_values)} "
                             "on duplicate key update "
                             "pass_count = pass_count + values(pass_count), "
                             "flaky_count = flaky_count + values(flaky_count), "
                             "disabled_count = disabled_count + values(disabled_count), "
                             "fail_count = fail_count + values(fail_count), "
                             "filtered_pass_count = filtered_pass_count + "
                             "values(filtered_pass_count), "
                             "time = time + values(time)")
            self.db.commit()
        else:
            print("WARN: Received invalid input file")
        print("updating test results")
        results = t_t.result_rows()
        if not results:
            # junit is empty, so these results will be ignored
            print("WARN: Empty result file")
            return
        values = [f"({self.q(test_id)}, {self.q(hardware)}, {self.q(arch)}, {self.build_num}, "
                  f"{self.q(status)}, {self.q(filtered_status)}, {pass_count}, {fail_count}, "
                  f"{flaky_miss_count}, {disabled}, {time}, {self.q(stdout, True)}, "
                  f"{self.q(stderr, True)})"
                  for (test_id, hardware, arch, status, filtered_status, pass_count, fail_count,
                       flaky_miss_count, disabled, time, stdout, stderr) in results]
        values = ", ".join(values)
        self.cur.execute(f"insert ignore into result (test_id, hardware, arch, build_id, status, "
                         "filtered_status, pass_count, fail_count, flaky_miss_count, disabled, "
                         f"time, stdout, stderr) values {values}")
        self.db.commit()

        print("updating build statistics")
        self.cur.execute("select pass_count, flaky_count, disabled_count, "
                         "filtered_pass_count, fail_count "
                         f"from group_ where build_id={self.build_num} and "
                         f"test_id={self.q(md5('root'))}")
        (pass_count, flaky_count, disabled_count,
         filtered_pass_count, fail_count) = self.cur.fetchone()

        build_id = self.build_num
        build_name = self.q(self.build_name)
        start_time = self.q(self.build_start_time)
        end_time = self.q(self.build_end_time)
        url = self.q(self.url)
        result_path = self.q(self.result_path)
        self.cur.execute("replace into "
                         "build  ( build_id ,  build_name ,  pass_count ,  flaky_count, "
                         "  disabled_count,   filtered_pass_count ,  fail_count ,  start_time ,  "
                         "  end_time,   url,  result_path,  imported)"
                         f"values({build_id}, {build_name}, {pass_count}, {flaky_count}, "
                         f"{disabled_count}, {filtered_pass_count}, {fail_count}, {start_time}, "
                         f"{end_time}, {url}, {result_path}, FALSE )")
        self.db.commit()
        print("import done")

    def disconnect(self):
        """close the database connection"""
        self.db.close()

    def add_revisions(self, revisions):
        """Add the revisions data for the build to the revisions table"""
        print("adding revisions for build")
        for project in revisions:
            details = revisions[project]
            project = self.q(project)
            commit = self.q(details['commit'])
            author = self.q(details['author'].encode('utf-8'), True)
            description = self.q(details['description'].encode('utf-8'), True)
            sha = self.q(details['sha'][:12])
            build_id = self.build_num
            self.cur.execute("insert into "
                             "revision( project,   commit,   author,   description,"
                             "  sha,   build_id) "
                             f"values ({project}, {commit}, {author}, {description}, "
                             f"{sha}, {build_id})")
            self.db.commit()

    def set_imported(self):
        """set the 'imported' flag on the build database, which
        protects partially imported builds from being displayed in the
        results.  This method is called after all files in the tarball
        have been imported.
        """
        self.cur.execute(f"update build set imported=TRUE where build_id={self.build_num}")
        self.db.commit()


def main():
    """main routine"""
    results_path = '/tmp/mesa_ci_results'
    if not os.path.exists(results_path):
        os.mkdir(results_path)

    sql_host = 'localhost'
    if "SQL_DATABASE_HOST" in os.environ:
        sql_host = os.environ["SQL_DATABASE_HOST"]

    sql_user = 'jenkins'
    if "SQL_DATABASE_USER" in os.environ:
        sql_user = os.environ["SQL_DATABASE_USER"]

    sql_socket = None
    if "SQL_DATABASE_SOCK" in os.environ:
        sql_socket = os.environ["SQL_DATABASE_SOCK"]

    results_importer = ResultsImporter(results_path, sql_host=sql_host,
                                       sql_user=sql_user,
                                       sql_socket=sql_socket)
    # consume any existing tar files
    for tarfile in glob.glob(results_path + "/*.xz"):
        results_importer.consume(tarfile)
    while True:
        tarfile = results_importer.poll()
        results_importer.consume(tarfile)


if __name__ == "__main__":
    main()
